Ctrl + .
🔥 Auto imports missing declarations or code fixes suggested from vs code
(Search missing declarations in node modules, and other project files)

Bonus: Ctrl + space
(Key combination to force Intellisense to show up.)
